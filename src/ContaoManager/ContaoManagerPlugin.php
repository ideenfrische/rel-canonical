<?php


namespace ideenfrische\CanonicalBundle\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;

/**
 * Plugin for the Contao Manager.
 *
 * @author Glen Langer (ideenfrische)
 */
class ContaoManagerPlugin implements BundlePluginInterface
{
    /**
     * {@inheritdoc}
     */
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create('ideenfrische\CanonicalBundle\ideenfrischeCanonicalBundle')
                ->setLoadAfter([
                  \Contao\CoreBundle\ContaoCoreBundle::class,
                  \Contao\FaqBundle\ContaoFaqBundle::class,
                  \Contao\CalendarBundle\ContaoCalendarBundle::class,
                  \Contao\NewsBundle\ContaoNewsBundle::class,
                  'rel-canonical'
                ]),
        ];
    }
}
